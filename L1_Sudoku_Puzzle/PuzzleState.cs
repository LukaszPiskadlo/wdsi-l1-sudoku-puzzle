﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1_Sudoku_Puzzle
{
    class PuzzleState : State
    {
        private int gridSize;
        private string id;
        private int[,] grid;
        private int lastRow;
        private int lastCol;
        private Heuristics heuristic;

        public override string ID => id;

        public int[,] Grid { get => grid; set => grid = value; }
        public int GridSize { get => gridSize; set => gridSize = value; }
        public Heuristics Heuristic => heuristic;

        public enum Direction
        {
            Up,
            Down,
            Left,
            Right
        }

        public enum Heuristics
        {
            MisplacedTiles,
            Manhattan
        }

        public PuzzleState(string puzzlePattern, int size, Heuristics heuristic) : base()
        {
            gridSize = size;
            this.heuristic = heuristic;
            string[] splittedPattern = puzzlePattern.Split(',');

            if (splittedPattern.Length != gridSize * gridSize)
            {
                throw new ArgumentException("puzzlePattern posiada niewlasciwa dlugosc!");
            }

            id = puzzlePattern;

            grid = new int[gridSize, gridSize];
            for (int i = 0; i < gridSize; ++i)
            {
                for (int j = 0; j < gridSize; ++j)
                {
                    grid[i, j] = int.Parse(splittedPattern[i * gridSize + j]);

                    if (grid[i, j] == 0)
                    {
                        lastRow = i;
                        lastCol = j;
                    }
                }
            }

            g = 0;
            h = ComputeHeuristicGrade();
        }

        public PuzzleState(PuzzleState parent, Direction dir, int x, int y) : base(parent)
        {
            gridSize = parent.GridSize;
            grid = new int[gridSize, gridSize];
            Array.Copy(parent.grid, grid, grid.Length);

            heuristic = parent.heuristic;
            lastRow = x;
            lastCol = y;

            switch (dir)
            {
                case Direction.Up:
                    if (x > 0)
                    {
                        grid[x, y] = grid[x - 1, y];
                        grid[x - 1, y] = 0;
                    }
                    break;
                case Direction.Down:
                    if (x < gridSize - 1)
                    {
                        grid[x, y] = grid[x + 1, y];
                        grid[x + 1, y] = 0;
                    }
                    break;
                case Direction.Left:
                    if (y > 0)
                    {
                        grid[x, y] = grid[x, y - 1];
                        grid[x, y - 1] = 0;
                    }
                    break;
                case Direction.Right:
                    if (y < gridSize - 1)
                    {
                        grid[x, y] = grid[x, y + 1];
                        grid[x, y + 1] = 0;
                    }
                    break;
                default:
                    break;
            }

            StringBuilder builder = new StringBuilder();
            foreach (var item in grid)
            {
                builder.Append(item);
                builder.Append(',');
            }
            id = builder.ToString().Remove(builder.Length - 1);

            g = parent.g + 1;
            h = ComputeHeuristicGrade();
        }

        public override double ComputeHeuristicGrade()
        {
            if (heuristic == Heuristics.Manhattan)
                return Manhattan();
            else
                return MisplacedTiles();
        }

        public void Print()
        {
            int maxDigits = (int)Math.Floor(Math.Log10(gridSize * gridSize) + 1);

            for (int i = 0; i < gridSize; i++)
            {
                for (int j = 0; j < gridSize; j++)
                {
                    int digits = 0;

                    if (grid[i, j] == 0)
                    {
                        Console.Write("  ");
                        digits = 1;
                    }
                    else
                    {
                        if (i == lastRow && j == lastCol)
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.Write(grid[i, j] + " ");
                            Console.ResetColor();
                        }
                        else
                            Console.Write(grid[i, j] + " ");

                        digits = (int)Math.Floor(Math.Log10(grid[i, j]) + 1);
                    }

                    for (int n = 0; n < maxDigits - digits; n++)
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        private int MisplacedTiles()
        {
            int sum = 0;
            for (int i = 0; i < gridSize; ++i)
            {
                for (int j = 0; j < gridSize; ++j)
                {
                    if (grid[i, j] != 0 && grid[i, j] != (i * gridSize + j))
                        sum++;
                }
            }
            return sum;
        }

        private int Manhattan()
        {
            int sum = 0;
            for (int i = 0; i < gridSize; ++i)
            {
                for (int j = 0; j < gridSize; ++j)
                {
                    if (grid[i, j] != 0)
                    {
                        sum += Math.Abs(i - grid[i, j] / gridSize);
                        sum += Math.Abs(j - grid[i, j] % gridSize);
                    }
                }
            }
            return sum;
        }
    }
}
