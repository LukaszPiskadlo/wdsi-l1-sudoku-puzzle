﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1_Sudoku_Puzzle
{
    class Program
    {
        private const string SUDOKU_FILE = "sudoku.txt";

        static void Main(string[] args)
        {
            Program program = new Program();
            program.Menu();
            Console.WriteLine("\n\nPress any key to quit");
            Console.ReadKey();
        }

        private void Menu()
        {
            Console.WriteLine("Wybierz opcje:");
            Console.WriteLine("1. Sudoku\n2. Puzzle");
            var key = Console.ReadKey();
            Console.WriteLine();

            switch (key.Key)
            {
                case ConsoleKey.D1:
                    string sudoku = SelectSudoku();
                    SolveSudoku(sudoku, SudokuState.Heuristics.Sum);
                    break;
                case ConsoleKey.D2:
                    Console.Write("Input puzzle size [3]: ");
                    int puzzleSize = GetUserInput(3);

                    Console.Write("Input number of puzzles to generate [100]: ");
                    int puzzleAmount = GetUserInput(100);

                    Console.Write("Input scramble amount [1000]: ");
                    int puzzleScrambleAmount = GetUserInput(1000);

                    bool printEnabled = GetPrintOption();

                    Console.WriteLine("\nManhattan:");
                    SolvePuzzle(puzzleSize, puzzleAmount, puzzleScrambleAmount, PuzzleState.Heuristics.Manhattan, printEnabled);

                    Console.WriteLine("\nMisplacedTiles:");
                    SolvePuzzle(puzzleSize, puzzleAmount, puzzleScrambleAmount, PuzzleState.Heuristics.MisplacedTiles, printEnabled);
                    break;
                default:
                    break;
            }
        }

        private void SolveSudoku(string sudoku, SudokuState.Heuristics heuristic)
        {
            SudokuState startState = new SudokuState(sudoku, heuristic);
            SudokuSearch searcher = new SudokuSearch(startState);
            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();
            searcher.DoSearch();
            stopwatch.Stop();

            IState state = searcher.Solutions[0];

            List<SudokuState> solutionPath = new List<SudokuState>();

            while (state != null)
            {
                solutionPath.Add((SudokuState)state);
                state = state.Parent;
            }
            solutionPath.Reverse();

            foreach (SudokuState s in solutionPath)
            {
                s.Print();
            }

            Console.WriteLine($"Time elapsed: {stopwatch.Elapsed}");
            Console.WriteLine($"States in Open: {searcher.Open.Count}, states in Close: {searcher.Closed.Count}");
        }

        private void SolvePuzzle(int puzzleSize, int puzzleAmount, int scrambleAmount,
            PuzzleState.Heuristics heuristic, bool printEnabled)
        {

            List<int> closedStates = new List<int>();
            List<int> openStates = new List<int>();
            List<double> pathsLength = new List<double>();
            List<TimeSpan> times = new List<TimeSpan>();

            Stopwatch totalTime = new Stopwatch();

            for (int i = 0; i < puzzleAmount; i++)
            {
                string puzzle = GeneratePuzzle(puzzleSize, scrambleAmount);
                PuzzleState startState = new PuzzleState(puzzle, puzzleSize, heuristic);
                PuzzleSearch searcher = new PuzzleSearch(startState);
                Stopwatch stopwatch = new Stopwatch();

                totalTime.Start();
                stopwatch.Start();
                searcher.DoSearch();
                stopwatch.Stop();
                totalTime.Stop();

                IState state = searcher.Solutions[0];

                closedStates.Add(searcher.Closed.Count);
                openStates.Add(searcher.Open.Count);
                pathsLength.Add(state.G);
                times.Add(stopwatch.Elapsed);

                if (printEnabled)
                {
                    List<PuzzleState> solutionPath = new List<PuzzleState>();

                    while (state != null)
                    {
                        solutionPath.Add((PuzzleState)state);
                        state = state.Parent;
                    }
                    solutionPath.Reverse();


                    foreach (PuzzleState s in solutionPath)
                    {
                        s.Print();
                    }

                    Console.WriteLine($"Solved {i + 1} of {puzzleAmount}");
                    Console.WriteLine($"Time elapsed: {stopwatch.Elapsed}");
                    Console.WriteLine($"States in Open: {searcher.Open.Count}, states in Close: {searcher.Closed.Count}");
                    Console.WriteLine($"Path length (g): {solutionPath.Last().G}");
                }
                else
                {
                    Console.Write($"\rSolved {i + 1} of {puzzleAmount}");
                }
            }

            Console.WriteLine("\nTotal stats:");
            Console.WriteLine($"Total time: {totalTime.Elapsed}");
            Console.WriteLine($"Average time: {new TimeSpan(Convert.ToInt64(times.Average(ts => ts.Ticks)))}");
            Console.WriteLine($"States in Open (avg): {openStates.Average()}, states in Close (avg): {closedStates.Average()}");
            Console.WriteLine($"Average path length (g): {pathsLength.Average()}");
        }

        private string GeneratePuzzle(int size, int scrambleAmount)
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < size * size; i++)
            {
                builder.Append(i.ToString());
                builder.Append(',');
            }
            string solvedPuzzle = builder.ToString().Remove(builder.Length - 1);

            Random rand = new Random();
            PuzzleState state = new PuzzleState(solvedPuzzle, size, PuzzleState.Heuristics.Manhattan);
            for (int s = 0; s < scrambleAmount; s++)
            {
                for (int i = 0; i < state.GridSize; ++i)
                {
                    for (int j = 0; j < state.GridSize; ++j)
                    {
                        if (state.Grid[i, j] == 0)
                        {
                            PuzzleState.Direction dir;
                            do
                            {
                                dir = (PuzzleState.Direction)rand.Next(Enum.GetNames(typeof(PuzzleState.Direction)).Length + 1);
                            }
                            while (!((i > 0 && dir == PuzzleState.Direction.Up) ||
                                (i < state.GridSize - 1 && dir == PuzzleState.Direction.Down) ||
                                (j > 0 && dir == PuzzleState.Direction.Left) ||
                                (j < state.GridSize - 1 && dir == PuzzleState.Direction.Right)));

                            state = new PuzzleState(state, dir, i, j);
                        }
                    }
                }
            }
            return state.ID;
        }

        private int GetUserInput(int defValue)
        {
            string input = Console.ReadLine().Trim();
            int value;
            if (String.IsNullOrEmpty(input))
                value = defValue;
            else
                value = int.Parse(input);

            return value;
        }

        private bool GetPrintOption()
        {
            Console.Write("Print each state? (y / N): ");
            var key = Console.ReadKey();
            Console.WriteLine();

            bool enabled;
            switch (key.Key)
            {
                case ConsoleKey.Y:
                    enabled = true;
                    break;
                default:
                    enabled = false;
                    break;

            }

            return enabled;
        }

        string SelectSudoku()
        {
            List<string> sudokuPattern = File.ReadLines(SUDOKU_FILE).ToList();

            for (int i = 0; i < sudokuPattern.Count; i++)
            {
                PrintSudoku(sudokuPattern[i], i, sudokuPattern.Count);
            }

            Console.Write("Select sudoku [2]: ");
            int number = GetUserInput(2);

            return sudokuPattern[number];
        }

        private void PrintSudoku(string sudokuPattern, int number, int total)
        {
            const int SMALL_GRID_SIZE = 3;
            const int GRID_SIZE = SMALL_GRID_SIZE * SMALL_GRID_SIZE;

            int[,] grid = new int[GRID_SIZE, GRID_SIZE];
            for (int i = 0; i < GRID_SIZE; ++i)
            {
                for (int j = 0; j < GRID_SIZE; ++j)
                {
                    grid[i, j] = sudokuPattern[i * GRID_SIZE + j] - '0';
                }
            }

            int columnHeight = GRID_SIZE + 4;
            int rightMargin = 2 * (Console.BufferWidth / (GRID_SIZE * 2));
            int columnWidth = GRID_SIZE * 2 + rightMargin;
            int numberOfColumns = Console.BufferWidth / columnWidth;
            int offset = number % numberOfColumns;
            int leftPosition = offset * columnWidth;

            Console.SetCursorPosition(leftPosition, Console.CursorTop);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"{number}.");
            Console.ResetColor();
            Console.SetCursorPosition(leftPosition, Console.CursorTop);

            for (int i = 0; i < GRID_SIZE; i++)
            {
                for (int j = 0; j < GRID_SIZE; j++)
                {
                    if (j % SMALL_GRID_SIZE == 0 && j > 0)
                        Console.Write("| ");

                    if (grid[i, j] == 0)
                        Console.Write("  ");
                    else
                    {
                        Console.Write(grid[i, j] + " ");
                    }
                }

                Console.WriteLine();
                Console.SetCursorPosition(leftPosition, Console.CursorTop);

                if ((i + 1) % SMALL_GRID_SIZE == 0 && i < GRID_SIZE - 1)
                {
                    for (int j = 0; j < GRID_SIZE; j++)
                    {
                        if (j % SMALL_GRID_SIZE == 0 && j > 0)
                            Console.Write("+ ");

                        Console.Write("- ");
                    }

                    Console.WriteLine();
                    Console.SetCursorPosition(leftPosition, Console.CursorTop);
                }
            }

            Console.WriteLine();
            if (offset < numberOfColumns - 1 && number < total - 1)
                Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop - columnHeight);
        }
    }
}
