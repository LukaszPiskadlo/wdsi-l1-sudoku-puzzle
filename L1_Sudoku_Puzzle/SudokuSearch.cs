﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1_Sudoku_Puzzle
{
    public class SudokuSearch : AStarSearch
    {
        public SudokuSearch(IState initialState, bool isStopAfterFirstSolution, bool isStopAfterSecondSolution)
            : base(initialState, isStopAfterFirstSolution, isStopAfterSecondSolution)
        {
        }

        public SudokuSearch(SudokuState state) : base(state, true, true)
        {
        }

        protected override void buildChildren(IState parent)
        {
            int currentBest = int.MaxValue;
            int bestI = 0;
            int bestJ = 0;

            SudokuState state = parent as SudokuState;
            for (int i = 0; i < SudokuState.GRID_SIZE; ++i)
            {
                for (int j = 0; j < SudokuState.GRID_SIZE; ++j)
                {
                    if (state.Grid[i, j] == 0)
                    {
                        int possibleChildren = state.GetNumberOfPossibleChildren(i, j);
                        if (currentBest > possibleChildren)
                        {
                            currentBest = possibleChildren;
                            bestI = i;
                            bestJ = j;
                        }
                    }
                }
            }

            for (int k = 1; k <= SudokuState.GRID_SIZE; ++k)
            {
                SudokuState child = new SudokuState(state, k, bestI, bestJ);
                parent.Children.Add(child);
            }
        }

        protected override bool isSolution(IState state)
        {
            return state.H == 0.0;
        }
    }
}
