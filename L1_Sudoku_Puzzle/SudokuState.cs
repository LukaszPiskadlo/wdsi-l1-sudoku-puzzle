﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1_Sudoku_Puzzle
{
    public class SudokuState : State
    {
        public const int SMALL_GRID_SIZE = 3;
        public const int GRID_SIZE = SMALL_GRID_SIZE * SMALL_GRID_SIZE;

        private string id;
        private int[,] grid;
        private int lastCol;
        private int lastRow;
        private Heuristics heuristic;

        public override string ID => id;

        public int[,] Grid { get => grid; set => grid = value; }

        public enum Heuristics
        {
            Sum,
            Naive
        }

        public SudokuState(string sudokuPattern, Heuristics heuristic) : base()
        {
            if (sudokuPattern.Length != GRID_SIZE * GRID_SIZE)
            {
                throw new ArgumentException("sudokuPattern posiada niewlasciwa dlugosc!");
            }

            id = sudokuPattern;
            lastRow = 0;
            lastCol = 0;
            this.heuristic = heuristic;

            grid = new int[GRID_SIZE, GRID_SIZE];
            for (int i = 0; i < GRID_SIZE; ++i)
            {
                for (int j = 0; j < GRID_SIZE; ++j)
                {
                    grid[i, j] = sudokuPattern[i * GRID_SIZE + j] - '0';
                }
            }

            h = ComputeHeuristicGrade();
        }

        public SudokuState(SudokuState parent, int newValue, int x, int y) : base(parent)
        {
            grid = new int[GRID_SIZE, GRID_SIZE];
            Array.Copy(parent.grid, grid, grid.Length);

            grid[x, y] = newValue;
            lastRow = x;
            lastCol = y;
            heuristic = parent.heuristic;

            StringBuilder builder = new StringBuilder(parent.id);
            builder[x * GRID_SIZE + y] = (char)(newValue + '0');
            id = builder.ToString();

            h = ComputeHeuristicGrade();
        }

        public override double ComputeHeuristicGrade()
        {
            double grade = 0;
            if (!IsValid())
                grade = double.PositiveInfinity;
            else if (heuristic == Heuristics.Sum)
                grade = GetNumberOfAllPossibleChildren();
            else
                grade = GetNumberOfEmptyFields();

            return grade;
        }

        public void Print()
        {
            for (int i = 0; i < GRID_SIZE; i++)
            {
                for (int j = 0; j < GRID_SIZE; j++)
                {
                    if (j % SMALL_GRID_SIZE == 0 && j > 0)
                        Console.Write("| ");

                    if (grid[i, j] == 0)
                        Console.Write("  ");
                    else
                    {
                        if (i == lastRow && j == lastCol)
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.Write(grid[i, j] + " ");
                            Console.ResetColor();
                        }
                        else
                            Console.Write(grid[i, j] + " ");
                    }
                }
                Console.WriteLine();

                if ((i + 1) % SMALL_GRID_SIZE == 0 && i < GRID_SIZE - 1)
                {
                    for (int j = 0; j < GRID_SIZE; j++)
                    {
                        if (j % SMALL_GRID_SIZE == 0 && j > 0)
                            Console.Write("+ ");

                        Console.Write("- ");
                    }
                    Console.WriteLine();
                }
            }
            Console.WriteLine();
        }

        public bool IsValid()
        {
            return ValidateSudoku(grid);
        }

        public bool IsValid(int i, int j, int newValue)
        {
            int[,] table = new int[GRID_SIZE, GRID_SIZE];
            Array.Copy(grid, table, table.Length);
            table[i, j] = newValue;

            return ValidateSudoku(table);
        }

        private bool ValidateSudoku(int[,] table)
        {
            for (int i = 0; i < GRID_SIZE; i++)
            {
                int rowFlag = 0;
                int colFlag = 0;
                int squareFlag = 0;

                for (int j = 0; j < GRID_SIZE; j++)
                {
                    if (table[i, j] != 0)
                    {
                        int value = 1 << table[i, j];
                        if ((rowFlag & value) != 0)
                            return false;
                        rowFlag |= value;
                    }

                    if (table[j, i] != 0)
                    {
                        int value = 1 << table[j, i];
                        if ((colFlag & value) != 0)
                            return false;
                        colFlag |= value;
                    }

                    int squareRow = (i / SMALL_GRID_SIZE) * SMALL_GRID_SIZE + j / SMALL_GRID_SIZE;
                    int squareCol = i * SMALL_GRID_SIZE % GRID_SIZE + j % SMALL_GRID_SIZE;
                    if (table[squareRow, squareCol] != 0)
                    {
                        int value = 1 << table[squareRow, squareCol];
                        if ((squareFlag & value) != 0)
                            return false;
                        squareFlag |= value;
                    }
                }
            }
            return true;
        }

        public int GetNumberOfPossibleChildren(int i, int j)
        {
            int sum = 0;
            for (int k = 1; k <= GRID_SIZE; ++k)
            {
                if (IsValid(i, j, k) && grid[i, j] == 0)
                    sum++;
            }

            return sum;
        }

        private int GetNumberOfAllPossibleChildren()
        {
            int sum = 0;
            for (int i = 0; i < GRID_SIZE; i++)
            {
                for (int j = 0; j < GRID_SIZE; j++)
                {
                    if (grid[i, j] == 0)
                        sum += GetNumberOfPossibleChildren(i, j);
                }
            }

            return sum;
        }

        private int GetNumberOfEmptyFields()
        {
            return id.Count(x => x == '0');
        }
    }
}
