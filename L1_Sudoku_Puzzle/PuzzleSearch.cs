﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1_Sudoku_Puzzle
{
    class PuzzleSearch : AStarSearch
    {
        public PuzzleSearch(IState initialState, bool isStopAfterFirstSolution, bool isStopAfterSecondSolution)
            : base(initialState, isStopAfterFirstSolution, isStopAfterSecondSolution)
        {
        }

        public PuzzleSearch(PuzzleState state) : base(state, true, true)
        {
        }

        protected override void buildChildren(IState parent)
        {
            PuzzleState state = parent as PuzzleState;
            for (int i = 0; i < state.GridSize; ++i)
            {
                for (int j = 0; j < state.GridSize; ++j)
                {
                    if (state.Grid[i, j] == 0)
                    {
                        if (i > 0)
                        {
                            PuzzleState child = new PuzzleState(state, PuzzleState.Direction.Up, i, j);
                            parent.Children.Add(child);
                        }
                        if (i < state.GridSize - 1)
                        {
                            PuzzleState child = new PuzzleState(state, PuzzleState.Direction.Down, i, j);
                            parent.Children.Add(child);
                        }
                        if (j > 0)
                        {
                            PuzzleState child = new PuzzleState(state, PuzzleState.Direction.Left, i, j);
                            parent.Children.Add(child);
                        }
                        if (j < state.GridSize - 1)
                        {
                            PuzzleState child = new PuzzleState(state, PuzzleState.Direction.Right, i, j);
                            parent.Children.Add(child);
                        }
                    }
                }
            }
        }

        protected override bool isSolution(IState state)
        {
            return state.H == 0.0;
        }
    }
}
